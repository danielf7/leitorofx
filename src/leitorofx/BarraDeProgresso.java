/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leitorofx;

import controllers.Progresso;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author daniel.freitas
 */
public class BarraDeProgresso extends JFrame implements Runnable {

    private JProgressBar pbLeitura;
    private JLabel lbCarregar;
    private Thread td;
    private JLabel lbProcentagem;
    private String caminho;
    private JLabel banco,conta,dtinicial,dtfinal,saldofinal,saldoinit;
    private DefaultTableModel dm;
    
    public BarraDeProgresso(String caminho,
            JLabel banco,
            JLabel conta,
            JLabel datainicial,
            JLabel datafinal,
            JLabel saldofinal,
            JLabel saldoinit,
            DefaultTableModel dm) {
        
        this.caminho = caminho;
        this.banco = banco;
        this.conta = conta;
        this.dtinicial = datainicial;
        this.dtfinal = datafinal;
        this.saldofinal = saldofinal;
        this.saldoinit = saldoinit;
        this.dm = dm;
        
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            // handle exception
        }

        inicializarComponentes();
        definirEventos();
    }

    @Override
    public void run() {
        abrir(caminho,banco,conta,dtinicial,dtfinal,saldofinal,saldoinit,dm);
    }
    
    public void start() {
        if(td == null){
            td = new Thread(this);
            td.start();
        }
    }

    private void inicializarComponentes() {
        setTitle("Carregando");
        setLayout(null);
        setBounds(0, 0, 200, 150);

        lbCarregar = new JLabel();
        lbCarregar.setBounds(10, 10, 100, 25);
        add(lbCarregar);

        pbLeitura = new JProgressBar();
        pbLeitura.setBounds(10, 50, 150, 25);
        pbLeitura.setValue(0);
        pbLeitura.setStringPainted(true);
        add(pbLeitura);
    }

    private void definirEventos() {
        Progresso p = new Progresso(pbLeitura, this,caminho,banco,conta,dtinicial,dtfinal,saldofinal,saldoinit,dm);
        p.start();
    }

    public void abrir(String caminho,
            JLabel banco,
            JLabel conta,
            JLabel datainicial,
            JLabel datafinal,
            JLabel saldofinal,
            JLabel saldoinit,
            DefaultTableModel dm) {
        BarraDeProgresso b = new BarraDeProgresso(caminho,
                banco,
                conta,
                datainicial,
                datafinal,
                saldofinal,
                saldoinit,
                dm);

        b.setUndecorated(true);
        b.setVisible(true);
        b.setLocationRelativeTo(null);
        b.setDefaultCloseOperation(EXIT_ON_CLOSE);
        b.setResizable(false);
    }
}