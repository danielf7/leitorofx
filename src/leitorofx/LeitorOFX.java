/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leitorofx;

import controllers.OFXControl;
import helpers.ColorirTabela;
import helpers.Suporte;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import models.BancoConta;
import models.OFX;

/**
 *
 * @author daniel.freitas
 */
public class LeitorOFX extends JFrame {

    private JLabel lbArquivo, lbPesquisa, lbBanco, lbConta, lbDataInicial, lbDataFinal, lbSaldoFinal, lbSaldoInicial, lbCarregando;
    private JTextField tfArquivo, tfPesquisa, tfAno;
    private JButton btSelecionar, btExportar;
    private JScrollPane scCorpo;
    private JTable tbCorpo;
    private JComboBox cbMes;
    private DefaultTableModel dm;
    private Color azul = new Color(102, 194, 255);

    public LeitorOFX() {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            // handle exception
        }
        inicializarComponentes();
        definirEventos();

    }

    public static void main(String[] args) {
        // TODO code application logic here
        LeitorOFX l = new LeitorOFX();
        l.setVisible(true);
        l.setResizable(false);
        l.setLocationRelativeTo(null);
        l.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void inicializarComponentes() {
        setTitle("Leitor de OFX");
        setBounds(0, 0, 600, 700);
        setLayout(null);
        this.getContentPane().setBackground(azul);

        lbArquivo = new JLabel("Selecione um arquivo");
        lbArquivo.setBounds(10, 10, 250, 25);
        lbArquivo.setFont(new Font("Arial", Font.PLAIN, 15));
        lbArquivo.setForeground(Color.WHITE);
        add(lbArquivo);

        tfArquivo = new JTextField();
        tfArquivo.setBounds(10, 40, 300, 30);
        add(tfArquivo);

        Object[] meses = {
            "Janeiro",
            "Fevereiro",
            "Março",
            "Abril",
            "Maio",
            "Junho",
            "Julho",
            "Agosto",
            "Setembro",
            "Outubro",
            "Novembro",
            "Dezembro"};

        cbMes = new JComboBox(meses);
        cbMes.setBounds(315, 40, 95, 30);
        cbMes.setSelectedIndex(4);
        add(cbMes);

        tfAno = new JTextField("2017");
        tfAno.setBounds(315, 80, 95, 30);
        add(tfAno);

        lbPesquisa = new JLabel("Pesquisa...");
        lbPesquisa.setBounds(10, 125, 200, 25);
        lbPesquisa.setFont(new Font("Arial", Font.PLAIN, 15));
        lbPesquisa.setForeground(Color.WHITE);
        add(lbPesquisa);

        tfPesquisa = new JTextField();
        tfPesquisa.setBounds(10, 150, 200, 30);
        add(tfPesquisa);

        lbCarregando = new JLabel("");
        lbCarregando.setBounds(250, 150, 250, 25);
        lbCarregando.setFont(new Font("Arial", Font.PLAIN, 15));
        lbCarregando.setForeground(Color.WHITE);
        add(lbCarregando);

        btSelecionar = new JButton("Abrir");
        btSelecionar.setBounds(415, 40, 80, 25);
        btSelecionar.setBorder(null);
        btSelecionar.setForeground(Color.WHITE);
        btSelecionar.setBorderPainted(false);
        btSelecionar.setFocusPainted(false);
        btSelecionar.setContentAreaFilled(false);
        btSelecionar.setCursor(new Cursor(12));
        btSelecionar.setFont(new Font("Arial", Font.ITALIC, 14));
        add(btSelecionar);

        btExportar = new JButton("Gerar");
        btExportar.setBounds(500, 40, 90, 25);
        btExportar.setBackground(Color.WHITE);
        btExportar.setBorderPainted(false);
        btExportar.setFocusPainted(false);
        btExportar.setContentAreaFilled(false);
        btExportar.setForeground(Color.white);
        btExportar.setCursor(new Cursor(12));
        btExportar.setFont(new Font("Arial", Font.ITALIC, 14));
        add(btExportar);

        scCorpo = new JScrollPane();
        scCorpo.setBounds(10, 200, 575, 400);

        dm = new DefaultTableModel(new Object[]{"Data", "Historico", "Valor", "Saldo"},
                0);

        tbCorpo = new JTable(dm);

        tbCorpo.setRowHeight(25);

        tbCorpo.getColumnModel().getColumn(0).setPreferredWidth(40);
        tbCorpo.getColumnModel().getColumn(1).setPreferredWidth(300);
        tbCorpo.getColumnModel().getColumn(2).setPreferredWidth(50);
        tbCorpo.getColumnModel().getColumn(3).setPreferredWidth(50);

        scCorpo.setViewportView(tbCorpo);
        add(scCorpo);

        lbBanco = new JLabel("Banco: ");
        lbBanco.setBounds(10, 610, 200, 25);
        lbBanco.setFont(new Font("Arial", Font.PLAIN, 13));
        lbBanco.setForeground(Color.WHITE);
        add(lbBanco);

        lbConta = new JLabel("Conta: ");
        lbConta.setBounds(10, 640, 200, 25);
        lbConta.setFont(new Font("Arial", Font.PLAIN, 13));
        lbConta.setForeground(Color.WHITE);
        add(lbConta);

        lbDataInicial = new JLabel("Data Inicial: ");
        lbDataInicial.setBounds(210, 610, 200, 25);
        lbDataInicial.setFont(new Font("Arial", Font.PLAIN, 13));
        lbDataInicial.setForeground(Color.WHITE);
        add(lbDataInicial);

        lbDataFinal = new JLabel("Data Final: ");
        lbDataFinal.setBounds(210, 640, 200, 25);
        lbDataFinal.setFont(new Font("Arial", Font.PLAIN, 13));
        lbDataFinal.setForeground(Color.WHITE);
        add(lbDataFinal);

        lbSaldoInicial = new JLabel("Saldo Inicial: ");
        lbSaldoInicial.setBounds(400, 610, 300, 25);
        lbSaldoInicial.setFont(new Font("Arial", Font.BOLD, 15));
        lbSaldoInicial.setForeground(Color.WHITE);
        add(lbSaldoInicial);

        lbSaldoFinal = new JLabel("Saldo Final: ");
        lbSaldoFinal.setBounds(400, 640, 300, 25);
        lbSaldoFinal.setFont(new Font("Arial", Font.BOLD, 15));
        lbSaldoFinal.setForeground(Color.WHITE);
        add(lbSaldoFinal);

    }

    private void definirEventos() {
        Color vermelho = new Color(255, 204, 204);
        Color verde = new Color(179, 255, 217);
        tbCorpo.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                setHorizontalAlignment(JLabel.CENTER);
                table.getColumnModel().getColumn(0).setCellRenderer(this);
                table.getColumnModel().getColumn(1).setCellRenderer(this);
                table.getColumnModel().getColumn(2).setCellRenderer(this);
                table.getColumnModel().getColumn(3).setCellRenderer(this);
                String valor = (String) table.getModel().getValueAt(row, 2);
                if (valor.contains("-")) {
                    c.setBackground(vermelho);
                } else if (valor.equals("0,0") || valor.equals("0")) {
                    c.setBackground(Color.WHITE);
                } else {
                    c.setBackground(verde);
                }

                return c;
            }
        });

        btSelecionar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String url = Suporte.abrirArquivoOFX();
                tfArquivo.setText(url);
            }
        });

        btExportar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                lbCarregando.setText("Lendo Arquivo...");
                String url = tfArquivo.getText();
                if (url.equals("")) {
                    JOptionPane.showMessageDialog(null, "Ops! Selecione um arquivo!");
                } else {

                    String a = url.substring(url.length() - 3, url.length());
                    if (a.equals("ofx") || a.equals("OFX")) {

                        limparCampos();
                        dm.setRowCount(0);
//                        BarraDeProgresso b = new BarraDeProgresso(url, lbBanco, lbConta, lbDataInicial, lbDataFinal, lbSaldoFinal, lbSaldoInicial, dm);
//                        b.start();
                        String mes = String.valueOf(cbMes.getSelectedIndex() + 1);
                        if (mes.length() == 1) {
                            mes = "0" + mes;
                        }
                        ArrayList<OFX> lista = OFXControl.gerarLista(tfArquivo.getText(), lbBanco, lbConta, lbDataInicial, lbDataFinal, lbSaldoFinal, lbSaldoInicial, lbCarregando, mes, tfAno.getText());
                        if (lista != null) {
                            for (OFX ofx : lista) {

                                dm.addRow(new Object[]{
                                    Suporte.formatData(ofx.getData()),
                                    ofx.getHistorico(),
                                    ofx.getValor().replace(".", ","),
                                    String.valueOf(ofx.getSaldo()).replace(".", ",")
                                });
                            }
                        } else {
                            lbSaldoFinal.setText("Saldo Final:");
                            int res = JOptionPane.showConfirmDialog(null, "Saldo inicial referente ao período não localizado "
                                    + "ou o arquivo não \ncontem registros da competência selecionada.\n\n"
                                    + "Deseja gravar o saldo inicial referente ao mês do arquivo selecionado?");

                            if (res == 0) {
                                GravarSaldoGUI g = new GravarSaldoGUI(BancoConta.conta, BancoConta.banco);
                                g.setVisible(true);
                                g.setResizable(false);
                                g.setLocationRelativeTo(null);
                            }

                        }

                    } else {
                        System.out.println(a);
                        JOptionPane.showMessageDialog(null, "Ops! O arquivo selecionado não é um ofx!");
                    }
                }
            }
        });

        tfPesquisa.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                listarResultados(tfPesquisa.getText());
            }
        });
    }

    public void limparCampos() {
        lbBanco.setText("Banco: ");
        lbConta.setText("Conta: ");
        lbDataFinal.setText("Data Final:");
        lbDataInicial.setText("Data Inicial:");
    }

    public void listarResultados(String txt) {
        dm.setRowCount(0);
        for (OFX ofx : OFXControl.perquisar(txt)) {
            dm.addRow(new Object[]{
                Suporte.formatData(ofx.getData()),
                ofx.getHistorico(),
                ofx.getValor().replace(".", ","),
                String.valueOf(ofx.getSaldo()).replace(".", ",")
            });
        }
    }
}
