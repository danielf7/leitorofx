/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leitorofx;

import controllers.RegistroCTRL;
import helpers.Suporte;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;

/**
 *
 * @author daniel.freitas
 */
public class GravarSaldoGUI extends JFrame{
    private JTextField tfMes,tfAno,tfSaldo;
    private JLabel lbMes,lbAno,lbSaldo;
    private JButton btSalvar,btCancelar;
    private String conta,banco;
    
    public GravarSaldoGUI(String conta,String banco) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            // handle exception
        }
        this.conta = conta;
        this.banco = banco;
        
        inicializarComponentes();
        definirEventos();
    }

    private void inicializarComponentes() {
        setTitle("Saldo Inicial");
        setBounds(0,0,230,140);
        setLayout(null);
        
        lbMes = new JLabel("Mês");
        lbMes.setBounds(10,10,50,25);
        add(lbMes);
        
        tfMes = new JTextField();
        tfMes.setBounds(10,35,45,25);
        add(tfMes);
        
        
        lbAno = new JLabel("Ano");
        lbAno.setBounds(60,10,45,25);
        add(lbAno);
        
        tfAno = new JTextField("2017");
        tfAno.setBounds(60,35,50,25);
        add(tfAno);
        
        lbSaldo = new JLabel("Saldo");
        lbSaldo.setBounds(115,10,100,25);
        add(lbSaldo);
        
        tfSaldo = new JTextField();
        tfSaldo.setBounds(115,35,100,25);
        add(tfSaldo);
        
        btCancelar = new JButton("Cancelar");
        btCancelar.setBounds(10,70,100,25);
        add(btCancelar);
        
        btSalvar = new JButton("Salvar");
        btSalvar.setBounds(115,70,100,25);
        add(btSalvar);
        
    }

    private void definirEventos() {
        btCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        
        btSalvar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String bc = banco+conta.replace("-", "").replace(".", "");
                if(RegistroCTRL.inserir(Integer.parseInt(tfMes.getText()), Integer.parseInt(tfAno.getText()),bc, Double.parseDouble(tfSaldo.getText().replace(",", ".")))){
                    JOptionPane.showMessageDialog(null, "Arquivo gravado com sucesso");
                    dispose();
                }else{
                    JOptionPane.showMessageDialog(null, "Ops! Deu um errinho na hora de gravar o arquivo!");
                }
            }
        });
    }
}
