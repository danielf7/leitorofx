/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import java.awt.Color;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.UIManager;

/**
 *
 * @author daniel.freitas
 */
public class JSystemFileChooser extends JFrame{
    
    private static JProgressBar pbCarregando;

    public JSystemFileChooser() {
        inicializarComponentes();
        
    }
    
    
    
    public static void main(String[] args) {
         JSystemFileChooser fc = new  JSystemFileChooser();
         fc.setVisible(true);
         fc.setDefaultCloseOperation(EXIT_ON_CLOSE);
         fc.setResizable(false);
         fc.setLocationRelativeTo(null);
         update();
     }

    private void inicializarComponentes() {
        setTitle("Progress");
        setBounds(0,0,300,300);
        setLayout(null);
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            // handle exception
        }
        pbCarregando = new JProgressBar();
        pbCarregando.setBounds(10,40,200,25);
        pbCarregando.setValue(0);
        pbCarregando.setStringPainted(true);
        add(pbCarregando);
    }
    
    public static void update(){
        int i = 0;
        
        while(i <= 100){
            pbCarregando.setValue(i);
            i++;
            try{
                Thread.sleep(10);
            }catch(Exception e){
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }
}
