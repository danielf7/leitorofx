/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import models.BancoConta;
import models.OFX;

/**
 *
 * @author daniel.freitas
 */
public class Suporte {

    private static Preferences pref = Preferences.userRoot();
    private static String path = pref.get("DEFAULT_PATH", "");

    public static String abrirArquivoOFX() {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            // handle exception
        }
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File(path));
        int resp = fc.showOpenDialog(fc);
        if (resp == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            fc.setCurrentDirectory(f);
            pref.put("DEFAULT_PATH", f.getAbsolutePath());
            return fc.getSelectedFile().getAbsolutePath();
        } else {
            return "";
        }
    }

    public static String formatData(String data) {
        String ano = "", mes = "", dia = "";
        ano = data.substring(0, 4);
        mes = data.substring(4, 6);
        dia = data.substring(6, 8);

        return dia + "/" + mes + "/" + ano;
    }

    public static String bancoPorNumero(String id) {
        switch (id) {
            case "001":
                return "Banco do Brasil";
            case "0104":
                return "Caixa Economica Federal";
            case "004":
                return "Banco do Nordeste";
            case "0237":
                return "Banco Bradesco";
            case "036":
                return "Banco Bradesco";
            case "0204":
                return "Banco Bradesco";
            case "0394":
                return "Banco Bradesco";
            case "0341":
                return "Banco Itau";
            case "0184":
                return "Banco Itau";
            case "0479":
                return "Banco Itau";
            case "047":
                return "Banco Itau";
            case "033":
                return "Banco Santander";
            case "0422":
                return "Banco Safra";
            default:
                return "Banco indefinido";
        }
    }

    public static String buscarSaldoInicial(String conta, String agencia) {
        String caminho = BancoConta.CAMINHO + conta + agencia + ".txt";
        File saldo = new File(caminho);
        if (saldo.exists()) {
            return caminho;
        } else {
            return "";
        }
    }

    public static String lerArquivo(String caminho) {
        String content = "";
        BufferedReader in = null;
        File f = new File(caminho);
        if(f.exists()){
            try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(caminho), "iso-8859-1"));
            
            String str;

            while ((str = in.readLine()) != null) {
                content += str + "\n";
            }

            in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Suporte.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Suporte.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Suporte.class.getName()).log(Level.SEVERE, null, ex);
        }
        return content;
        }else{
            return "Arquivo não existe";
        }
        
    }

    public static boolean gravarSaldo(String mes, String ano, String saldo,String cb) {
        String c = BancoConta.CAMINHO+cb+".txt";

        if (c.equals("")) {
            JOptionPane.showMessageDialog(null, "Selecione um arquivo!", "Aviso", 2);
        } else {
            try {
                c = c.replace(".ofx", ".csv");
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(c, true)));
                String cs = ano + ";" + mes + ";" + saldo;
                
                writer.println(cs);

                writer.close();
                return true;
            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            } catch (UnsupportedEncodingException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }
        return false;
    }

    public static double buscarSaldo(String mes, String ano, String arquivo) {
        double sd = -999999999;
        String txt[] = arquivo.split("\n");
        String saldo[] = new String[3];
        for (int i = 0; i < txt.length; i++) {
            if (txt[i].contains(ano+";"+mes)) {
                saldo = txt[i].split(";");
            }
        }
        
        if(saldo[2] != null){
            sd = Double.parseDouble(saldo[2]);
        }
        
        return sd;
    }
    
    public static double calcularSaldoFinal(ArrayList<OFX> l, double saldoInicial){
        DecimalFormat df = new DecimalFormat("################.##");
        for(OFX o : l){
            System.out.println("Valor: "+o.getValor());
            saldoInicial += Double.parseDouble(o.getValor().replace(",", "."));
        }
        System.out.println(saldoInicial);
        return saldoInicial;
    }
    
    public static String ajustarValor(String v){
        System.out.println("Saldo finalzinho: " + v);
        int i = v.indexOf(".")+3;
        System.out.println("Posicao do ponto: "+ i);
        try{
            return v.substring(0,i).replace(".", ",");
        }catch(StringIndexOutOfBoundsException e){
            
        }
        return v;
    }
}
