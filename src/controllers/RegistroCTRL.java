/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import DAO.RegistroDAO;
import java.util.ArrayList;
import models.Registro;

/**
 *
 * @author daniel.freitas
 */
public class RegistroCTRL {
    static RegistroDAO f = new RegistroDAO();
    
    public static boolean inserir(int mes, int ano,String bc,double saldo){
        return f.inserir(mes, ano, bc,saldo);
    }
    
    public static ArrayList<Registro> listarRegistros(String bc, int mes, int ano){
        return f.listarRegistros(mes, ano, bc);
    }
    
    public static Registro getRegistro(String bc, int mes, int ano){
        return f.getRegistro(mes, ano, bc);
    }
}
