/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import helpers.Suporte;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import leitorofx.GravarSaldoGUI;
import models.BancoConta;
import models.OFX;
import models.Registro;

/**
 *
 * @author daniel.freitas
 */
public class OFXControl {

    private static ArrayList<OFX> lista = new ArrayList<>();
    private static double valor = 0;
    static String bancoid = "";
    static String cnt = "";

    public static ArrayList<OFX> gerarLista(String caminho, JLabel banco, JLabel conta, JLabel dtinicial, JLabel dtfinal, JLabel saldofinal, JLabel saldoinit, JLabel carregar, String mes,String ano) {
        String c = "", content = "";

        BufferedReader in;
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(caminho), "iso-8859-1"));

            c = c.replace(".html", ".csv");
            String str;
            while ((str = in.readLine()) != null) {
                content += str + "\n";
            }
            in.close();

            //System.out.println(content);
            String txt[] = content.split("<STMTTRN>");
            double saldof = 0;

            String cabecalho[] = content.split("\n");

            for (int i = 0; i < cabecalho.length; i++) {
                cabecalho[i] = cabecalho[i].trim();

                if (cabecalho[i].contains("<ORG>")) {
                    banco.setText("Banco: " + cabecalho[i].replace("<ORG>", "").replace("</ORG>", ""));
                } else if (cabecalho[i].contains("<ACCTID>")) {
                    cnt = (cabecalho[i].replace("<ACCTID>", "").replace("</ACCTID>", ""));
                    conta.setText("Conta: " + cnt);
                } else if (cabecalho[i].contains("<DTEND>")) {
                    dtfinal.setText("Data Final: " + Suporte.formatData(cabecalho[i].replace("<DTEND>", "").replace("</DTEND>", "")));
                } else if (cabecalho[i].contains("<BANKID>")) {
                    bancoid = (cabecalho[i].replace("<BANKID>", "").replace("</BANKID>", ""));
                } else if (cabecalho[i].contains("<DTSTART>")) {
                    dtinicial.setText("Data Inicial: " + Suporte.formatData(cabecalho[i].replace("<DTSTART>", "").replace("</DTSTART>", "")));
                } else if (cabecalho[i].contains("<BALAMT>")) {
                    saldofinal.setText("Saldo Final: " + cabecalho[i].replace("<BALAMT>", "").replace("</BALAMT>", "").replace(".", ","));
                    String tempsaldo = cabecalho[i].replace("<BALAMT>", "").replace("</BALAMT>", "").replace(",", ".");
                }
            }
            String arquivo = Suporte.lerArquivo(BancoConta.CAMINHO + bancoid + cnt.replace("-", "").replace(".", "") + ".txt");
            
            
            
            String bc = bancoid + cnt.replace("-", "").replace(".", "");
            Registro r = RegistroCTRL.getRegistro(bc, Integer.parseInt(mes), Integer.parseInt(ano));
            System.out.println(r.getSaldo()+";"+r.getBc()+";"+r.getAno()+";"+r.getMes());
            if (r.getBc() == null) {
                BancoConta.banco = bancoid;
                BancoConta.conta = cnt.replace("-", "").replace(".", "");
                
                return null;
            }
            saldof = r.getSaldo();
            if (banco.getText().length() == 7) {
                banco.setText("Banco: " + Suporte.bancoPorNumero(bancoid));
            }
            lista.clear();
            valor = 0;
            DecimalFormat df = new DecimalFormat("################.##");
            String dt2 = "", tempdt2 = "";
            //System.out.println(saldof);

            for (int i = 1; i < txt.length; i++) {
                OFX ofx = new OFX();

                String obj[] = txt[i].split("\n");
                for (int j = 0; j < obj.length; j++) {
                    obj[j] = obj[j].trim();
                    if (obj[j].contains("<DTPOSTED>")) {
                        tempdt2 = obj[j].replace("<DTPOSTED>", "").replace("</DTPOSTED>", "");
                        if (dt2.equals("")) {
                            dt2 = tempdt2;
                        }
                    }
                    if (obj[j].contains("<TRNAMT>")) {
                        ofx.setValor(obj[j].replace("<TRNAMT>", "").replace("</TRNAMT>", ""));
                        valor = Double.parseDouble(ofx.getValor().replace(",", "."));

//                    if (tempdt2.substring(4, 6).equals(mes)) {
//                        saldof += (valor * -1);
//                        //System.out.println(saldof);
//                    }
                    }
                }
            }
            saldoinit.setText(String.valueOf("Saldo Inicial: " + df.format(saldof)).replace(".", ","));
            OFX o = new OFX();
            o.setData("--------");
            o.setHistorico("SALDO INICIAL");
            o.setValor("0.0");
            o.setSaldo(Double.parseDouble(df.format(saldof).replace(",", ".")));
            //lista.add(o);
            double saldoinicial = saldof;
            String dt = "", tempdt = "";
            double val = 0;
            
            for (int i = 1; i < txt.length; i++) {
                OFX ofx = new OFX();
                System.out.println("entrar no carregamento da lista");
                String obj[] = txt[i].split("\n");

                for (int j = 0; j < obj.length; j++) {
                    obj[j] = obj[j].trim();
                    if (obj[j].contains("<DTPOSTED>")) {
                        tempdt = obj[j].replace("<DTPOSTED>", "").replace("</DTPOSTED>", "");

                        ofx.setData(obj[j].replace("<DTPOSTED>", "").replace("</DTPOSTED>", ""));

                    } else if (obj[j].contains("<TRNAMT>")) {

                        ofx.setValor(obj[j].replace("<TRNAMT>", "").replace("</TRNAMT>", ""));
                        valor = Double.parseDouble(ofx.getValor().replace(",", "."));
                        val = Double.parseDouble(ofx.getValor().replace(",", "."));
                        saldoinicial += valor;

                        //saldof += (valor);
                        //System.out.println(val);
                        //System.out.println("Saldo: "+df.format(saldoinicial));
                        ofx.setSaldo(Double.parseDouble(df.format(saldoinicial).replace(",", ".")));
                    } else if (obj[j].contains("<MEMO>")) {
                        ofx.setHistorico(obj[j].replace("<MEMO>", "").replace("</MEMO>", ""));
                    }

                }
//                    System.out.println("temp: "+tempdt.substring(4,6));
//                    System.out.println("dt: "+dt.substring(4,6));

                if (tempdt.substring(4, 6).equals(mes)) {
                    lista.add(ofx);
                }

            }
            OFX f = null;
            try{
                f = lista.get(lista.size()-1);
            }catch(ArrayIndexOutOfBoundsException e){
                return null;
            }
            
            System.out.println("asdf: "+f.getSaldo());
            saldofinal.setText("Saldo Final: "+Suporte.ajustarValor(String.valueOf(f.getSaldo())));
            RegistroCTRL.inserir(Integer.parseInt(mes)+1, Integer.parseInt(ano), bc, f.getSaldo());
            System.out.println(String.valueOf(Integer.parseInt(mes)+2));
            
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Caminho inválido!", "Erro", 0);
        } catch (IOException ex) {
            Logger.getLogger(OFXControl.class.getName()).log(Level.SEVERE, null, ex);
        }

        carregar.setText(
                "Leitura concluída com sucesso!");
        return lista;
    }

    public static void exportarArquivo(String c) {
        if (c.equals("")) {
            JOptionPane.showMessageDialog(null, "Selecione um arquivo!", "Aviso", 2);
        } else {
            try {
                c = c.replace(".ofx", ".csv");
                Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(c), "ISO-8859-1"));

                for (OFX ofx : lista) {
                    writer.write(Suporte.formatData(ofx.getData()) + ";" + ofx.getHistorico() + ";" + ofx.getValor() + ";" + ofx.getSaldo() + "\n");
                }
                writer.close();
                JOptionPane.showMessageDialog(null, "Arquivo exportado com sucesso!");
            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            } catch (UnsupportedEncodingException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }

    }

    public static ArrayList<OFX> perquisar(String txt) {
        ArrayList<OFX> resultados = new ArrayList<>();

        for (OFX ofx : lista) {
            if (ofx.getHistorico().toLowerCase().contains(txt.toLowerCase())
                    || ofx.getValor().replace(".", ",").contains(txt)
                    || String.valueOf(ofx.getSaldo()).replace(".", ",").contains(txt)
                    || Suporte.formatData(ofx.getData()).contains(txt)) {
                resultados.add(ofx);
            }
        }

        return resultados;
    }
}
