/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import helpers.Suporte;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.table.DefaultTableModel;
import models.OFX;

/**
 *
 * @author daniel.freitas
 */
public class Progresso implements Runnable {

    private static ArrayList<OFX> lista = new ArrayList<>();
    private static double valor = 0;
    private Thread t;
    private JProgressBar pbLeitura;
    private JFrame b;
    private String caminho;
    private JLabel banco, conta, dtinicial, dtfinal, saldofinal, saldoinit;
    private DefaultTableModel dm;

    public Progresso(
            JProgressBar pbLeitura,
            JFrame b,
            String caminho,
            JLabel banco,
            JLabel conta,
            JLabel datainicial,
            JLabel datafinal,
            JLabel saldofinal,
            JLabel saldoinit,
            DefaultTableModel dm) {
        this.pbLeitura = pbLeitura;
        this.b = b;
        this.caminho = caminho;
        this.banco = banco;
        this.conta = conta;
        this.dtinicial = datainicial;
        this.dtfinal = datafinal;
        this.saldofinal = saldofinal;
        this.saldoinit = saldoinit;
        this.dm = dm;
    }

    @Override
    public void run() {

        String c = "", content = "";

        BufferedReader in;
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(caminho), "iso-8859-1"));

            c = c.replace(".html", ".csv");
            String str;
            while ((str = in.readLine()) != null) {
                content += str + "\n";
            }
            in.close();

            //System.out.println(content);
            String txt[] = content.split("<STMTTRN>");
            double saldof = 0;
            String bancoid = "";
            String cabecalho[] = content.split("\n");

            for (int i = 0; i < cabecalho.length; i++) {
                cabecalho[i] = cabecalho[i].trim();

                if (cabecalho[i].contains("<ORG>")) {
                    banco.setText("Banco: " + cabecalho[i].replace("<ORG>", "").replace("</ORG>", ""));
                } else if (cabecalho[i].contains("<ACCTID>")) {
                    conta.setText("Conta: " + cabecalho[i].replace("<ACCTID>", "").replace("</ACCTID>", ""));
                } else if (cabecalho[i].contains("<DTEND>")) {
                    dtfinal.setText("Data Final: " + Suporte.formatData(cabecalho[i].replace("<DTEND>", "").replace("</DTEND>", "")));
                } else if (cabecalho[i].contains("<BANKID>")) {
                    bancoid = (cabecalho[i].replace("<BANKID>", "").replace("</BANKID>", ""));
                } else if (cabecalho[i].contains("<DTSTART>")) {
                    dtinicial.setText("Data Inicial: " + Suporte.formatData(cabecalho[i].replace("<DTSTART>", "").replace("</DTSTART>", "")));
                } else if (cabecalho[i].contains("<BALAMT>")) {
                    saldofinal.setText("Saldo Final: " + cabecalho[i].replace("<BALAMT>", "").replace("</BALAMT>", "").replace(".", ","));
                    String tempsaldo = cabecalho[i].replace("<BALAMT>", "").replace("</BALAMT>", "").replace(",", ".");
                    if (!tempsaldo.equals("")) {
                        saldof = Double.parseDouble(tempsaldo);
                    } else {
                        saldof = 0;
                        saldofinal.setText("Saldo Final: " + String.valueOf(saldof).replace(".", ","));
                    }

                }
            }
            if (banco.getText().length() == 7) {
                banco.setText("Banco: " + Suporte.bancoPorNumero(bancoid));
            }
            lista.clear();
            valor = 0;
            DecimalFormat df = new DecimalFormat("################.##");
            for (int i = 1; i < txt.length; i++) {
                OFX ofx = new OFX();

                String obj[] = txt[i].split("\n");
                for (int j = 0; j < obj.length; j++) {
                    obj[j] = obj[j].trim();
                    if (obj[j].contains("<TRNAMT>")) {
                        ofx.setValor(obj[j].replace("<TRNAMT>", "").replace("</TRNAMT>", ""));
                        valor = Double.parseDouble(ofx.getValor().replace(",", "."));
                        saldof += (valor * -1);

                    }
                }
            }
            saldoinit.setText(String.valueOf("Saldo Inicial: " + df.format(saldof)).replace(".", ","));
            OFX o = new OFX();
            o.setData("--------");
            o.setHistorico("SALDO INICIAL");
            o.setValor("0.0");
            o.setSaldo(Double.parseDouble(df.format(saldof).replace(",", ".")));
            lista.add(o);
            double saldoinicial = saldof;
            int addvalor = 0;
            dm.setRowCount(0);
            for (int i = 1; i < txt.length; i++) {
                OFX ofx = new OFX();

                String obj[] = txt[i].split("\n");

                for (int j = 0; j < obj.length; j++) {
                    obj[j] = obj[j].trim();
                    if (obj[j].contains("<DTPOSTED>")) {
                        ofx.setData(obj[j].replace("<DTPOSTED>", "").replace("</DTPOSTED>", ""));
                    } else if (obj[j].contains("<TRNAMT>")) {

                        ofx.setValor(obj[j].replace("<TRNAMT>", "").replace("</TRNAMT>", ""));
                        valor = Double.parseDouble(ofx.getValor().replace(",", "."));
                        saldoinicial += valor;

                        //System.out.println("Saldo: "+df.format(saldoinicial));
                        ofx.setSaldo(Double.parseDouble(df.format(saldoinicial).replace(",", ".")));
                    } else if (obj[j].contains("<MEMO>")) {
                        ofx.setHistorico(obj[j].replace("<MEMO>", "").replace("</MEMO>", ""));
                    }
                }

                //lista.add(ofx);
                dm.addRow(new Object[]{
                    Suporte.formatData(ofx.getData()),
                    ofx.getHistorico(),
                    ofx.getValor().replace(".", ","),
                    String.valueOf(ofx.getSaldo()).replace(".", ",")
                });

                addvalor = Math.round((i * 100) / txt.length) + 1;
                
                pbLeitura.setValue(addvalor);
                try {
                    Thread.sleep(5);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Progresso.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (addvalor == 100) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                b.dispose();
            }
            }
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Caminho inválido!", "Erro", 0);
        } catch (IOException ex) {
            Logger.getLogger(OFXControl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void start() {
        if (t == null) {
            t = new Thread(this);
            t.start();
        }
    }
}
