/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Registro;

/**
 *
 * @author daniel.freitas
 */
public class RegistroDAO {

    private PreparedStatement stm;
    private ResultSet rs;
    private Connection con;

    public RegistroDAO() {
        try {
            this.con = conexao.abreConexao();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public ArrayList<Registro> listarRegistros(int mes, int ano, String bc) {
        ArrayList<Registro> lista = new ArrayList<>();
        String query = "select * from tb_registro where "
                + "reg_banco_conta = '" + bc + "' and "
                + "reg_mes = " + mes + " and "
                + "reg_ano = " + ano;
        try {
            stm = con.prepareStatement(query);
            rs = stm.executeQuery();

            while (rs.next()) {
                Registro r = new Registro();
                r.setId(rs.getInt("reg_id"));
                r.setMes(rs.getInt("reg_mes"));
                r.setAno(rs.getInt("reg_ano"));
                r.setSaldo(rs.getDouble("reg_saldo"));
                r.setBc(rs.getString("reg_banco_conta"));

                lista.add(r);
            }
            return lista;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public Registro getRegistro(int mes, int ano, String bc) {
        Registro r = new Registro();
        String query = "select * from tb_registro where "
                + "reg_banco_conta = '" + bc + "' and "
                + "reg_mes = " + mes + " and "
                + "reg_ano = " + ano;
        try {
            stm = con.prepareStatement(query);
            rs = stm.executeQuery();

            while (rs.next()) {

                r.setId(rs.getInt("reg_id"));
                r.setMes(rs.getInt("reg_mes"));
                r.setAno(rs.getInt("reg_ano"));
                r.setSaldo(rs.getDouble("reg_saldo"));
                r.setBc(rs.getString("reg_banco_conta"));


            }
            return r;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public boolean inserir(int mes, int ano, String bc, double saldo) {
        String sql = "insert tb_registro values ('" + bc + "'," + mes + "," + ano + "," + saldo + ")";
        try {
            stm = con.prepareStatement(sql);
            return !stm.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
